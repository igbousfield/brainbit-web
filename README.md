# Brainbit

BrainBit EEG Headset JavaScript Library (using Web Bluetooth)

## Running the demo app

WEB Bluetooth API requires HTTPS
```
npm install
webpack
```
## Installation
1) Install: 
```
npm install brainbit-client
```
Usage:
```
import BrainbitClient from 'brainbit-client';
```
3) You can include build file from /dist directory into your HTML/JS file
3) Or you can use source files from /src directory, in this case you should add RXJS module to dependencies.
## Usage example
```
var brainbitClient = new BrainbitClient();

const connect = async () => {
  brainbitClient.connectionStatus.subscribe((status) => {
    console.log(status ? 'Connected!' : 'Disconnected');
  });
  try {
    await brainbitClient.connect();
    brainbitClient.statusData.subscribe((data) => {
      console.log('statusData', data);
    });
    brainbitClient.eventMarkers.subscribe((event) => {
      console.log(event);
    });
    brainbitClient.eegStream.subscribe((data) => {
      console.log(data);
    });
    brainbitClient.resistanceData.subscribe((data) => {
      console.log(data);
    });
  } catch (err) {
    console.error('Connection failed', err);
  }
};

const startEEG = () => {
  const startEEGStatus = await brainbitClient.startEEGStream();
  console.log('current status', startEEGStatus);
};
```

## Methods
- `async connect()`
is used for device connection. It returns a Promise that resolves to true or false value. 
- `async checkStatus()` 
checks the current status. It returns a Promice that resolves to the object with the next fields:
```
{
  status: {
    {
      name: 'string', // status name, one of NSS2_STATUS_INVALID, NSS2_STATUS_STOPED, NSS2_STATUS_SIGNAL, NSS2_STATUS_RESIST, NSS2_STATUS_BOOTLOADER_NEED
      value: 0, // status number
      message: 'string', // status description
    }
  },
  cmdError: {
    name: 'string', // error name, one of NSS2_ERROR_NOERROR, NSS2_ERROR_LEN, NSS2_ERROR_MODE
    value: 0, // error number
    message: 'string', // error message
  },
  batteryCharge: 45, // batary charge in %
  firmwareVersion: 8 
}
```
- `async startEEGStream()` starts EEG stream. It returns a Promice that resolves to the Status object with the next fields:
```
 {
    name: 'NSS2_STATUS_SIGNAL', 
    value: 2,
    message: 'Signal measurement started'
  }
```
- `async stopEEGStream()` stops EEG stream. It returns a Promise that resolves to the Status object.
- `async startResistanceData()` starts resistance measuring, it returns a Promise that resolves to the Status object.
- `async stopResistanceData()` stops resistance measuring, it returns a Promise that resolves to the Status object.
- `async deviceInfo()` returns a Promise that resolves to deviceInfoObject with the next fields:
```
model
hardwareRevision
firmwareVersion
name
deviceId
state (CONNECTED | DISCONNECTED)
```
- `disconnect()` disconnects bluetooth device.

## How to read data
There are some Observable properties for subscribing to receive data. 
### EEG Data
```
 brainbitClient.eegStream.subscribe((data) => {
   console.log(data);
 });
```
EEG Data Object
```
{
  number: int, // a number of a data packet
  marker: boolean,
  val0_ch1: float, // the first value of the channel 1
  val0_ch2: float, // the first value of the channel 2
  val0_ch3: float, // the first value of the channel 3
  val0_ch4: float, // the first value of the channel 4
  val1_ch1: float, // the second value of the channel 1
  val1_ch2: float, // the second value of the channel 2
  val1_ch3: float, // the second value of the channel 3
  val1_ch4: float, // the second value of the channel 4
}
```

### Resistance Data
```
brainbitClient.resistanceData.subscribe((data) => {
  console.log(data);
});
```
Resistance Data Object
```
{
  resistanceCh1: float, // measured resistance value
  resistanceCh2: float,
  resistanceCh3: float,
  resistanceCh4: float
}
```
### Status Data
```
brainbitClient.statusData.subscribe((data) => {
  console.log('statusData', data);
});
```
Status Data Object
```
{
  status: {
    {
      name: 'string', // status name, one of NSS2_STATUS_INVALID, NSS2_STATUS_STOPED, NSS2_STATUS_SIGNAL, NSS2_STATUS_RESIST, NSS2_STATUS_BOOTLOADER_NEED
      value: 0, // status number
      message: 'string', // status description
    }
  },
  cmdError: {
    name: 'string', // error name, one of NSS2_ERROR_NOERROR, NSS2_ERROR_LEN, NSS2_ERROR_MODE
    value: 0, // error number
    message: 'string', // error message
  },
  batteryCharge: 45, // batary charge in %
  firmwareVersion: 8 
}
```
## Event Markers
For convenience, there is an eventMarkers stream included in BrainbitClient that you can use in order to introduce timestamped event markers into your project. Just subscribe to eventMarkers and use the injectMarker method with the value and optional timestamp of an event to send it through the stream.
```
async function main() {
    let client = new brainbitClient();
    client.eventMarkers.subscribe((event) => {
        console.log(event);
    });
    client.injectMarker("thought")
    client.injectMarker("one")
    client.injectMarker("smile")
}
```
