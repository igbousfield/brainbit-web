var brainbitClient = new BrainbitClient();
var chartSubscribes = {};

const connect = async () => {
  brainbitClient.connectionStatus.subscribe((status) => {
    document.getElementById('connectionStatus').innerHTML = status ? 'Connected!' : 'Disconnected';
    setAvailabilityFunctionalButtons(!status);
  });

  try {
    await brainbitClient.connect();

    /* brainbitClient.statusData.subscribe((data) => {
      console.log('statusData', data);
    });*/

    brainbitClient.eventMarkers.subscribe((event) => {
      document.getElementById('marker').innerHTML = `<p>Value: ${event.value}</p><p>Time: ${getTimeFromDate(event.timestamp)}</p>`;
    });

  } catch (err) {
    console.error('Connection failed', err);
  }
};

const eegStart = async () => {
  const startEEGStatus = await brainbitClient.startEEGStream();
  unsubscribeCharts();
  createChart('ch1');
  createChart('ch2');
  createChart('ch3');
  createChart('ch4');
  displayStatus(startEEGStatus);
};

const eegStop = async () => {
  const stopStatus = await brainbitClient.stopEEGStream();
  unsubscribeCharts();
  displayStatus(stopStatus);
};
const deviceInfo = async () => {
  await brainbitClient.deviceInfo().then((deviceInfo) => {
    let deviceInfoText = '';
    Object.keys(deviceInfo).forEach((field) => {
      deviceInfoText += `<p>${field}: ${deviceInfo[field]}</p>`;
    });
    document.getElementById('device_info').innerHTML = deviceInfoText;
  });
};

const injectMarker = async () => {
  await brainbitClient.injectMarker('test');
};

const checkStatus = async () => {
  const status = await brainbitClient.checkStatus();
  displayFullStatus(status);
};

const displayFullStatus = (status) => {
  let text = `<p>Status: ${status.status.message}</p><p>Error: ${status.cmdError.message}</p>
  <p>Battery Charge: ${status.batteryCharge}</p><p>Firmware Version: ${status.firmwareVersion}</p>`;
  document.getElementById('check_status').innerHTML = text;
};

const displayStatus = (status) => {
  let text = `<p>Status: ${status.message}</p>`;
  document.getElementById('check_status').innerHTML = text;
};

const filter = (filter, invar) => {
        let sumden = 0.0;
        let sumnum = 0.0;
        for (let i = 0; i < filter.order; i++) {
            sumden += filter.v[i] * filter.denominators[i];
            sumnum += filter.v[i] * filter.numerators[i];
            if (i < filter.order - 1) filter.v[i] = filter.v[i + 1];
        }
        filter.v[filter.order - 1] = invar - sumden;
        sumnum += filter.v[filter.order - 1] * filter.numerators[filter.order];
        return sumnum;
}
  

const createChart = (channel, type = 'eeg') => {
  Highcharts.chart(channel, {
    chart: {
      type: 'line',
      animation: Highcharts.svg, // don't animate in old IE
      marginRight: 10,
      events: {
        load: function () {
          var series = this.series[0];
          let lastRedraw = Date.now();
          if (type === 'eeg') {
            let highPassFilter = {
              order:2, 
              v:[0.0, 0.0], 
              denominators:[0.96508117389913495, -1.9644605802052322],
              numerators:[0.98238543852609173, -1.9647708770521835, 0.98238543852609173]
            }

            let lowPassFilter = {
              order:2, 
              v:[0.0, 0.0], 
              denominators:[0.34766539485172343, -0.98240579310839538],
              numerators:[0.091314900435831972, 0.18262980087166394, 0.091314900435831972]
            }  

            let bandStopFilter = {
              order:4, 
              v:[0.0, 0.0, 0.0, 0.0], 
              denominators:[0.7008967811884026, -0.94976030879978701, 1.9723023606063141, -1.136085493907057],
              numerators:[0.8370891905663449, -1.0429229013534211, 1.9990207606620285, -1.0429229013534211, 0.8370891905663449]
            } 
            chartSubscribes[`eeg_${channel}`] = brainbitClient.eegStream.subscribe((data) => {
              var x = (new Date()).getTime();
              var y = filter(bandStopFilter, filter(lowPassFilter, filter(highPassFilter, data['val0_' + channel])));
              var y_point2 = filter(bandStopFilter, filter(lowPassFilter, filter(highPassFilter, data['val1_' + channel])));
              // console.log('data', data);
              // console.log(channel, y);
              series.addPoint([x, y], false, true);
              var redraw = (Date.now() - lastRedraw) > 300;
              series.addPoint([Date.now(), y_point2], redraw, true);
              if (redraw) {
                lastRedraw = Date.now();
              }
            });
          } else if (type === 'resist') {
            chartSubscribes[channel] = brainbitClient.resistanceData.subscribe((data) => {
              var x = Date.now();
              var y = data[channel];
              console.log('data', data);
              console.log(channel, data[channel]);
              var redraw = (x - lastRedraw) > 300;
              series.addPoint([x, y], redraw, true);
              if (redraw) {
                lastRedraw = Date.now();
              }
            });
          }
        },
      },
    },
    title: {
      text: '',
    },
    xAxis: {
      type: 'datetime',
      tickPixelInterval: 150,
    },

    yAxis: {
      title: {
        text: '',
        enabled: false,
      },
    },
    tooltip: {
      enanled: false,
    },
    plotOptions: {
      line: {
        lineWidth: 1,
      },
    },
    legend: {
      enabled: false,
    },
    labels: {
      enebled: true,
    },
    exporting: {
      enabled: false,
    },

    series: [{
      name: 'Chanel 1 direct',
      data: (function () {
        // generate an array of flat data
        var data = [],
          date = new Date(),
          time = date.getTime(),
          i;

        for (i = -900; i <= 0; i += 1) {
          data.push({
            x: time + i,
            y: null,
          });
        }
        return data;
      }()),
    }],
  });
};

const resistanceStart = async () => {
  const startResistanceStatus = await brainbitClient.startResistanceData();
  unsubscribeCharts();
  createChart('resistanceCh1', 'resist');
  createChart('resistanceCh2', 'resist');
  createChart('resistanceCh3', 'resist');
  createChart('resistanceCh4', 'resist');
  displayStatus(startResistanceStatus);
};

const resistanceStop = async () => {
  const stopResistanceStatus = await brainbitClient.stopResistanceData();
  unsubscribeCharts();
  displayStatus(stopResistanceStatus);
};

const unsubscribeCharts = () => {
  Object.values(chartSubscribes).forEach(subscribe => subscribe.unsubscribe());
};

const pad = (num) => {
  return ("0"+num).slice(-2);
};

const getTimeFromDate = (timestamp) => {
  var date = new Date(timestamp);
  var hours = date.getHours();
  var minutes = date.getMinutes();
  var seconds = date.getSeconds();
  return pad(hours)+":"+pad(minutes)+":"+pad(seconds)
};

const setAvailabilityFunctionalButtons = (value) => {
  document.getElementById('startEEG').disabled = value;
  document.getElementById('stopEEG').disabled = value;
  document.getElementById('checkStatus').disabled = value;
  document.getElementById('startResistence').disabled = value;
  document.getElementById('stopResistanse').disabled = value;
  document.getElementById('info').disabled = value;
  document.getElementById('injectMarger').disabled = value;
};

window.onload = () => {
  setAvailabilityFunctionalButtons(true);
};
