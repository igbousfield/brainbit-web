import { merge, Subject } from 'rxjs';
import { map } from 'rxjs/operators';
import {
  getStartResistCommand,
  observableCharacteristic,
  parseSignal,
} from './brainbit-utils';
import { BLE_UUID_NSS2_SIGNAL_CHAR, DEVICE_MODE } from './static';

class BrainbitSubscribes {
  constructor(service, sendCommand, statusData) {
    this.service = service;
    this.sendCommand = sendCommand;
    this.signalStream = null;
    this.deviceMode = null;
    this.statusData = statusData;
    this.isResistCommandAccepted = true;
    this.resistanceData = null;
    this.eegData = null;
    this.getSignalStream = this.getSignalStream.bind(this);
    this.subscribeEegStream = this.subscribeEegStream.bind(this);
    this.subscribeResistance = this.subscribeResistance.bind(this);
  }

  async getSignalStream() {
    if (!this.signalStream) {
      const signalObservables = [];
      const signalChar = await this.service.getCharacteristic(BLE_UUID_NSS2_SIGNAL_CHAR);
      signalObservables.push(
        (await observableCharacteristic(signalChar)).pipe(
          map((data) => parseSignal(data.buffer, this.deviceMode)),
        ),
      );
      this.signalStream = merge(...signalObservables);
    }

    return this.signalStream;
  }

  async getEegStream() {
    this.deviceMode = DEVICE_MODE.SIGNAL;
    this.eegData = new Subject();
    await this.subscribeEegStream(this.eegData);
    return this.eegData;
  }

  async subscribeEegStream(subscriber) {
    (await this.getSignalStream()).subscribe((data) => {
      if (this.deviceMode !== DEVICE_MODE.SIGNAL) {
        return;
      }
      subscriber.next(data);
    });
  }

  async getResistanceData() {
    this.deviceMode = DEVICE_MODE.RESISTANCE;
    this.resistanceData = new Subject();
    await this.subscribeResistance();
    return this.resistanceData;
  }

  async subscribeResistance() {
    let inverse = false;
    let dataAcc = [];
    const resistancesPos = new Array(4).fill([]);
    const resistancesNeg = new Array(4).fill([]);
    const resistanceMultiplier = 2;
    let activeChannelIndex = 1;
    this.statusData.subscribe((status) => {
      if (status.status.name === 'NSS2_STATUS_RESIST') {
        this.isResistCommandAccepted = true;
      } else {
        this.isResistCommandAccepted = false;
        activeChannelIndex = 1;
        inverse = false;
      }
    });

    (await this.getSignalStream()).subscribe((data) => {
      if (!this.isResistCommandAccepted
        || data[`val0_ch${activeChannelIndex}`] === 0
        || data[`val1_ch${activeChannelIndex}`] === 0) {
        return;
      }
      dataAcc.push(data[`val0_ch${activeChannelIndex}`] * resistanceMultiplier);
      dataAcc.push(data[`val1_ch${activeChannelIndex}`] * resistanceMultiplier);
      if (dataAcc.length === 26) {
        if (inverse) {
          resistancesNeg[activeChannelIndex - 1] = dataAcc.slice(20, 25);
        } else {
          resistancesPos[activeChannelIndex - 1] = dataAcc.slice(20, 25);
        }
        dataAcc = [];
        if (activeChannelIndex === 4 && inverse) {
          const isInfinityCh1 = this.isInfinity([...resistancesPos[0], resistancesNeg[0]]);
          const isInfinityCh2 = this.isInfinity([...resistancesPos[1], resistancesNeg[1]]);
          const isInfinityCh3 = this.isInfinity([...resistancesPos[2], resistancesNeg[2]]);
          const isInfinityCh4 = this.isInfinity([...resistancesPos[3], resistancesNeg[3]]);
          if (this.resistanceData) {
            this.resistanceData.next({
              resistanceCh1: (!isInfinityCh1)
                ? this.calculateResistance(resistancesPos[0], resistancesNeg[0])
                : Infinity,
              resistanceCh2: (!isInfinityCh2)
                ? this.calculateResistance(resistancesPos[1], resistancesNeg[1])
                : Infinity,
              resistanceCh3: (!isInfinityCh3)
                ? this.calculateResistance(resistancesPos[2], resistancesNeg[2])
                : Infinity,
              resistanceCh4: (!isInfinityCh4)
                ? this.calculateResistance(resistancesPos[3], resistancesNeg[3])
                : Infinity,
            });
          }
        }
        if (this.isResistCommandAccepted) {
          if (inverse) {
            activeChannelIndex = activeChannelIndex >= 4 ? 1 : activeChannelIndex + 1;
          }
          inverse = !inverse;
          this.isResistCommandAccepted = false;
          this.sendCommand(getStartResistCommand(activeChannelIndex, inverse));
        }
      }
    });
  }

  calculateResistance(arrPos, arrNeg) {
    return Math.abs(
      arrPos.reduce((a, b) => a + b, 0) / (arrPos.length || 1)
      - arrNeg.reduce((a, b) => a + b, 0) / (arrNeg.length || 1),
    ) / 150e-9 / 4;
  }

  isInfinity(arr) {
    const inf = arr.find((item) => (item > 0.75 || item < -0.75));
    return !!inf;
  }

  setMode(deviceMode) {
    this.deviceMode = deviceMode;
  }
}

export default BrainbitSubscribes;
