import { fromEvent } from 'rxjs';
import { map, takeUntil } from 'rxjs/operators';
import { statuses, errors, DEVICE_MODE, commands } from './static';

const parseBatteryData = (byte) => {
  const binary = byte.toString(2);
  return parseInt(binary.slice(0, -1), 2);
};

const parsePacketNumber = (data) => ((data[0]) << 3) | ((data[1]) >> 5);

const parseUserMarker = (data) => ((((data[1]) & 0b00010000) >> 4) !== 0);

const parseSignalValuesShifts = (dataArray, signalValueMultiplier) => {
  const int8Arr = new Uint8Array(dataArray);
  const result = {
    number: parsePacketNumber(int8Arr),
    marker: parseUserMarker(int8Arr),
  };
  let value00 =
    ((int8Arr[1] & 0x0F) << 28)
    | (int8Arr[2] << 20)
    | (int8Arr[3] << 12)
    | (int8Arr[4] << 4);
  value00 /= 2048;

  let value01 =
    ((int8Arr[4] & 0x7F) << 25)
    | (int8Arr[5] << 17)
    | (int8Arr[6] << 9)
    | (int8Arr[7] << 1);
  value01 /= 2048;

  let value02 =
    ((int8Arr[6] & 0x03) << 30)
    | (int8Arr[7] << 22)
    | (int8Arr[8] << 14)
    | (int8Arr[9] << 6);
  value02 /= 2048;

  let value03 =
    ((int8Arr[9] & 0x1F) << 27)
    | (int8Arr[10] << 19)
    | (int8Arr[11] << 11);
  value03 /= 2048;

  let value10 =
    (int8Arr[12] << 24)
    | (int8Arr[13] << 16);
  value10 = value10 / 65536 + value00;

  let value11 =
    (int8Arr[14] << 24)
    | (int8Arr[15] << 16);
  value11 = value11 / 65536 + value01;

  let value12 =
    (int8Arr[16] << 24)
    | (int8Arr[17] << 16);
  value12 = value12 / 65536 + value02;

  let value13 =
    (int8Arr[18] << 24)
    | (int8Arr[19] << 16);
  value13 = value13 / 65536 + value03;

  result.val0_ch1 = value00 * signalValueMultiplier;
  result.val1_ch1 = value10 * signalValueMultiplier;

  result.val0_ch2 = value01 * signalValueMultiplier;
  result.val1_ch2 = value11 * signalValueMultiplier;

  result.val0_ch3 = value02 * signalValueMultiplier;
  result.val1_ch3 = value12 * signalValueMultiplier;

  result.val0_ch4 = value03 * signalValueMultiplier;
  result.val1_ch4 = value13 * signalValueMultiplier;
  return result;
};

export const decodeStatusResponse = (bytes) => {
  if (!bytes.length) return null;
  return {
    status: statuses.find((item) => (item.value === bytes[0])),
    cmdError: errors.find((item) => (item.value === bytes[1])),
    batteryCharge: parseBatteryData(bytes[2]),
    firmwareVersion: bytes[3],
  };
};

export const encodeCommand = (cmd) => ((Array.isArray(cmd)) ? new Uint8Array(cmd) : new Uint8Array([cmd]));

export async function observableCharacteristic(characteristic) {
  await characteristic.startNotifications();
  const disconnected = fromEvent(characteristic.service.device, 'gattserverdisconnected');
  return fromEvent(characteristic, 'characteristicvaluechanged').pipe(
    takeUntil(disconnected),
    map((event) => (event.target.value)),
  );
}

export const parseSignal = (data, mode) => {
  let signalValueMultiplier = 1;
  if (mode === DEVICE_MODE.SIGNAL || mode === DEVICE_MODE.RESISTANCE) {
    signalValueMultiplier = 2.4 / (0xFFFFF * 6);
  }
  return parseSignalValuesShifts(data, signalValueMultiplier);
};

export const getStartResistCommand = (chIndex = 1, isInverse = false) =>
  (commands.resist[`ch${chIndex}`][isInverse ? 'reversed' : 'direct']);

